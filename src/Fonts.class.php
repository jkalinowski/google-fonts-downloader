<?php
/*

  * "Google fonts" fonts downloader
  * @author TheTroll (Jakub Kalinowski)

*/

class Fonts {

  /*
    * multiexplode function
    * @params string ($delimiters), string ($string)
    * @return array
  */
  public static function multiexplode($delimiters, $string) {
    $ready = str_replace($delimiters, $delimiters[0], $string);
    $launch = explode($delimiters[0], $ready);

    return  $launch;
  }

  /*
    * getStringBetween function
    * @params string ($str), string ($from), string ($to)
    * @return string
  */
  public static function getStringBetween($str,$from,$to) {
  	$sub = substr($str, strpos($str,$from)+strlen($from),strlen($str));
  	return substr($sub,0,strpos($sub,$to));
  }

  /*
    * download and save function
    * @param string ($link)
    * @return on success: array
    * @return on error: array
  */
  public static function downloadFont($link) {
    $regex_url = "/(http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

    // Check if there is a url in the text
  	if( preg_match( $regex_url, $link, $url ) ) {

      $fonts_array = array();

      //get the font name
  		$font_name_prepare = explode("family=", $url[0]);
  		$font_name = Fonts::multiexplode(array(":", "&"), $font_name_prepare[1])[0];

      //create fonts folder if not exists
      if(!file_exists("fonts")) {
        mkdir("fonts");
      }

  		//create font file, if not exists
  		if(!file_exists("fonts/$font_name")) {
  			mkdir("fonts/$font_name");
  		}else {
  			//font already added by someone
            $fonts_array['error'] = 'Font already found!';
            return $fonts_array;
  		}

        //setup the css file
  		$css_file = "fonts/{$font_name}/$font_name.css";
  		$google_css = $url[0];
  		$google_css = rtrim($google_css, "'");

  		$ch = curl_init();
  		$fp = fopen($css_file, 'w+');
  		$ch = curl_init($google_css);
  		curl_setopt($ch, CURLOPT_TIMEOUT, 50);
  		curl_setopt($ch, CURLOPT_FILE, $fp);
  		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  		curl_exec($ch);
  		curl_close($ch);
  		fclose($fp);

        //add css file to array
		$fonts_array['FONT'] = $font_name;
		$fonts_array['CSS'] = "fonts/{$font_name}/$font_name.css";

  		//get the download css content
  		$css_file_contents = file_get_contents($css_file);

  		if(preg_match_all($regex_url, $css_file_contents, $fonts)) {

  			$fonts = $fonts[0];
            $css_file_array = explode("@font-face", $css_file_contents);

  			foreach($fonts as $i => $font) {
  				$font = rtrim($font, ")");

  				$font_file = explode("/", $font);
  				$font_file = array_pop($font_file);

  				// download and save font
  				$ch = curl_init();
  				$fp = fopen("fonts/{$font_name}/{$font_file}", 'w+');
  				$ch = curl_init($font);
  				curl_setopt($ch, CURLOPT_TIMEOUT, 50);
  				curl_setopt($ch, CURLOPT_FILE, $fp);
  				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  				curl_exec($ch);
  				curl_close($ch);
  				fclose($fp);

                //getting font type
  				foreach($css_file_array as $value) {
  					if(strpos($value, $font_file)) {
  						$from = "src:";
  						$to = "url";
  						$locals = Fonts::getStringBetween($value, $from, $to);

  						$sources = explode(",", $locals);
  						$font_type = Fonts::getStringBetween($sources[0], "'", "'");
  					}
  				}

  				$fonts_array['fonts'][] = array("fonts/{$font_name}/{$font_file}", $font_file, $font_type);

  				//css file replace content
  				$css_file_contents = str_replace($font, "../fonts/{$font_name}/{$font_file}", $css_file_contents);
  			}

            //css file update content and save
  			$fh = fopen($css_file, 'w+');
  			fwrite($fh, $css_file_contents);
  			fclose($fh);

            $fonts_array['success'] = true;

  			return $fonts_array;
  		}else {
  		    //on error - return error message

  		    fonts_array['error'] = 'Unknown error!';
  		}

        return $fonts_array;
  	}
  }

}
