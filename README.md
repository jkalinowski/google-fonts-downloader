# Google Fonts Downloader

A PHP class to download Google Fonts, so you can use them offline.

# Usage
*  Go to [Google Fonts](https://fonts.google.com), select a font you want and copy the <link ...> code
*  To download the font use the function downloadFont. As the parameter parse the coppied <link ...> from google fonts. On failure, you will get an array with an error message. On success, the function will return you an array with the font name and path to the css file, which you can include later in your page.

# Example

This code will download the font and display the returned array
```php
require 'Fonts.class.php';

$font_upload = Fonts::downloadFont("<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>");

echo '<pre>';
var_dump($font_upload);
echo '</pre>';
```

# Note

I use fonts directory structure like this:

```bash
fonts/
└── Open+Sans/
    ├── Open+Sans.css
    ├── mem5YaGs126MiZpBA-UN_r8OUuhs.ttf
    ├── mem5YaGs126MiZpBA-UN7rgOUuhs.ttf
    └── mem8YaGs126MiZpBA-UFVZ0e.ttf
```